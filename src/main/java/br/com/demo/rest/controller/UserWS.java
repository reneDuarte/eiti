package br.com.demo.rest.controller;

import br.com.demo.bean.Users;
import br.com.demo.service.ServiceUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author rene.duarte
 * <p>
 * metodos utiizados
 * '@POST'
 * '@DELETE'
 * '@PUT'
 * '@GET'
 * <p>
 * tomei essa desição para demostrar sus funcionalidades
 */

@RestController
@RequestMapping("/rest/comando")
public class UserWS {

    /**
     * injeção do spring
     */
    @Autowired
    private ServiceUser serviceUser;

    /**
     * rest salvar com as seguintes configuração:
     * POST
     * Path("/salvar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param objUsers
     * @return Retorno#fromJsomString()
     */

    @RequestMapping(method = RequestMethod.POST , value = "/salvar", consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retorno> salvar(@RequestBody Users objUsers) {

        return salvarUser(objUsers);
    }

    /**
     * rest listar com as seguintes configuração:
     * POST
     * Path("/listar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @return Retorno
     * @see Retorno
     */

    @RequestMapping(method = RequestMethod.POST , value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retorno> listar() {
        Retorno retorno = new Retorno();
        try {

            List lista = serviceUser.listarUsers();

            retorno.setCustomerList(lista);
            retorno.setSucesso(true);


        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }

        return  new ResponseEntity<>(retorno, HttpStatus.CREATED);
    }

    /**
     * rest deletar com as seguintes configuracao:
     * DELETE
     * Path("/delete/{nomeClasseID}")
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param ID (nome da classe e id EX:suaClasse@id)
     * @return Object
     */

    @RequestMapping(method = RequestMethod.DELETE , value = "/delete/{ID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retorno> deletar(@PathVariable String ID) {
        Retorno retorno = new Retorno();

        try {
            /**
             * verifica parametro inválido
             */

            Users users = new Users();
            users.setId(Integer.valueOf(ID));

            serviceUser.deletar(users);

            /**
             * configura o objeto de retorno do rest
             */
            retorno.setSucesso(true);


        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }

        return new ResponseEntity<>(retorno, HttpStatus.CREATED);
    }

    /**
     * rest editar com as seguintes configuração:
     * PUT
     * Path("/editar")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     *
     * @param objUsers
     * @return Object
     * @see Retorno
     */
    @RequestMapping(method = RequestMethod.PUT , value = "/editar", consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retorno> editar(@RequestBody Users objUsers) {

        return salvarUser(objUsers);
    }

    private ResponseEntity<Retorno> salvarUser(Users objUsers) {
        Retorno retorno = new Retorno();
        try {
            objUsers =  serviceUser.salvar(objUsers);

            /**
             * configura o objeto de retorno do rest
             */
            retorno.setCustomer(objUsers);
            retorno.setSucesso(true);
        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }
        return  new ResponseEntity<>(retorno, HttpStatus.CREATED);
    }

    /**
     * rest buscar por id com as seguintes configuração
     * GET
     * Path("/buscar/{nomeClasseID}")
     * Consumes(MediaType.APPLICATION_JSON)
     * Produces(MediaType.APPLICATION_JSON)
     ** @return Retorno#fromJsomString()
     */
    @RequestMapping(method = RequestMethod.GET , value = "/buscar/{nomeClasseID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getForId(@PathVariable String ID) {
        Retorno retorno = new Retorno();
        try {

            retorno.setSucesso(true);

        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());

        }

        return new ResponseEntity<>(retorno, HttpStatus.CREATED);
    }

}
