package br.com.demo.dao;



import br.com.demo.bean.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Dao<T extends Bean> extends JpaRepository<T, Integer> {


}
