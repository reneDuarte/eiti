package br.com.demo.dao;

import br.com.demo.bean.Bean;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DaoUsers<T extends Bean> extends  Dao {


    //Todo deixar nome da Classe dinamica
    @Override
    @Query("Select a from Users  as a")
    List<T> findAll();
}
