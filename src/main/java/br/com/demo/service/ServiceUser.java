package br.com.demo.service;


import br.com.demo.bean.Users;
import br.com.demo.dao.DaoUsers;
import br.com.demo.util.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * serviço do Spring
 */
@Service
public class ServiceUser {

    @Autowired
    private DaoUsers dao;

    public Users salvar(Users users) throws Exception {
        tratarData(users);
        tratarSenha(users);
        dao.save(users);

        return users;
    }
    private void tratarSenha(Users users) throws NoSuchAlgorithmException {
        if(users.getPassword() != null && !"".equals(users.getPassword())){
           users.setPassword(MD5.encode(users.getPassword()));
        }
    }
    private void tratarData(Users users) {
        if(users.getRegister_date() == null ){
            users.setRegister_date(new Date());
        }
    }

    public void deletar(Users objAtiv) throws Exception {
        dao.delete(objAtiv);
    }

    public List<Users> listarUsers() throws Exception {
        return  dao.findAll();
    }
}
