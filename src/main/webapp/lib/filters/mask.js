function criarUiMask(modulo){
	
	angular.module(modulo).directive("uiMask", function() {
	    return {
	        require: "ngModel",
	        link: function (scope, element, attrs, ctrl){
	        	
	        	
	        	var _format = function(valor) {
	        		
					var mascara = attrs.uiMask;
					
					if(valor)
						valor = valor.replace( /[^\d]*/g , '' );
					
					if(!mascara || !valor){
						return valor;
					}
					
					
					var ret = pegarRex(valor);
					 
					var strExp = ret.strExp;
					var replace = ret.replace;
					
					var exp = new RegExp(strExp);
					
					return valor.replace( exp , replace  )
					
				}
	        	
	        	
	        	function pegarRex(valor , replace , strExp){
	        		var cont9 = 0;
					//var replace= '';
					var seq = 0;
					//var strExp = '';
					var contG = 0;
	        		var mascara = attrs.uiMask;
	        		
	        		if(!replace)
	        			replace = '';
	        		if(!strExp)
	        			strExp = '';
	        		
	        		var tamanho = 0
	        		if(valor)	
	        			tamanho = valor.length
	        		
	        		for(var i in mascara){
	        			
	        			if(mascara[i]=='9'){

							cont9++;
							
							
							contG++
						
							if(valor && contG >= tamanho){
								break;	
							}
							
						}else{
							
							if(cont9 > 0){
								if(valor){
									seq++
									replace+='$' + seq
								};
								strExp +='([\\d]{'+ cont9 +'})'; 
							}
							cont9 = 0;
							if(valor){
								
								replace+=mascara[i]
							}else{
								var char = mascara[i];
								
								if(char == '(' || char == ')'){
									char = '\\'+ char
								}
								
								strExp+= char; 
							}
						} 
						
	        			
	        		}
	        		
	        		if(cont9 > 0){ //pegar o ultimo
						seq++
						if(valor)
							replace+='$' + seq;
						strExp +='([\\d]{'+ cont9 +'})'; 
					}
	        		
	        		if(!valor){
	        			attrs.ngPattern = angular.copy(strExp);
	        			attrs.pattern = attrs.ngPattern;
	        		}
	        		
	        		strExp +='([\\d]*)'; 
	        		return { 'replace': replace , 'strExp': strExp , 'ngPattern':attrs.ngPattern}
	        		
	        	}

	        	function criarValidador(){
	        		
	        		if (!ctrl) return;
	        		  attrs.$attr.ngPattern = 'ng-Pattern'	
	        		  	  
	        		  pegarRex();	  
	        	      var patternExp = attrs.ngPattern || attrs.pattern;
	        	      var regexp = new RegExp(patternExp);
	        	      
	        	      attrs.$observe('pattern', function(regex) {
	        	        if (angular.isString(regex) && regex.length > 0) {
	        	          regex = new RegExp('^' + regex + '$');
	        	        }

	        	        if (regex && !regex.test) {
	        	          throw minErr('ngPattern')('noregexp',
	        	            'Expected {0} to be a RegExp but was {1}. Element: {2}', patternExp,
	        	            regex, startingTag(elm));
	        	        }

	        	        regexp = regex || undefined;
	        	        ctrl.$validate();
	        	      });

	        	      ctrl.$validators.pattern = function(modelValue, viewValue) {
	        	        // HTML5 pattern constraint validates the input value, so we validate the viewValue
	        	    	 var ret = pegarRex();
	        	    	 var regexp = new RegExp(ret.ngPattern);
	        	    	 return ctrl.$isEmpty(viewValue) || angular.isUndefined(regexp) || regexp.test(viewValue);
	        	      };
	        	}
	        	
	        	
	        	if(ctrl && attrs.uiMask)
	        		criarValidador();
	        	
	            element.bind("keyup", function (){
	                ctrl.$setViewValue( _format(ctrl.$viewValue) );
	                ctrl.$render();
	            });
	        }
	    };
	});
	
}

