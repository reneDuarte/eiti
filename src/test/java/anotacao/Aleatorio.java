package anotacao;


import beans.TipoGeracao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Aleatorio {
    int tamanho() default 8;
    String mascara() default "";
    TipoGeracao tipoGeracao() default TipoGeracao.ALFANUMERICO;
    String msgErro() default  "";
}
