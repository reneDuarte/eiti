package page;

import dsl.DSL;
import org.openqa.selenium.WebDriver;

public class CadastroUsuarioPage {

    private DSL dsl;

    public CadastroUsuarioPage(WebDriver driver) {
        driver.get("http://127.0.0.1:8080/");
        driver.switchTo().frame("idConteudoIFrame");

        dsl = new DSL(driver);
    }

    public void setUsuario(String username) {
        dsl.escreverForName("username", username);
    }

    public String getUsuario() {
        return  dsl.getValueForName("username");
    }

    public void setSenha(String senha) {
        dsl.escreverForName("password", senha);
    }

    public void setNome(String nome) {
        dsl.escreverForName("name", nome);
    }

    public void setSobreNome(String sobreNome) {
        dsl.escreverForName("surname", sobreNome);
    }

    public void setTelefone(String telefone) {
        dsl.escreverForName("phone", telefone);
    }

    public void setEmail(String email) {
        dsl.escreverForName("email", email);
    }

    public void salvar() {
        dsl.clicarBotao("salvar");
    }


    public String getMsg() {
       return dsl.getValueForId("msg");
    }
}
