package telas;

import beans.UsuariaBean;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Parameterized.Parameter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import page.CadastroUsuarioPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class TesteCadastroUsuario {

    private CadastroUsuarioPage page;
    private WebDriver driver;


    @Before
    public void inicializa() {
        String caminho  = getClass().getResource("/driver/chromedriver.exe").getFile().replaceFirst("/" , "");
        System.setProperty("webdriver.chrome.driver", caminho );
        driver = new ChromeDriver();
        page = new CadastroUsuarioPage(driver);
    }

    @After
    public void finaliza() {
        driver.quit();
    }

    @Parameter
    public UsuariaBean usuariaBean;


    @Parameters
    public static Collection<Object[]> getCollection() {
        return getUsuarios();
    }

    private static List<Object[]> getUsuarios() {
        ArrayList arrayList = new ArrayList();
        //valida cadastro com suscesso
        arrayList.add(UsuariaBean.geraGeneric());

        {//valida usuario
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setUsuario("");
            arrayList.add(usuario);
        }
        {//valida senha
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setSenha("");
            arrayList.add(usuario);
        }

        {//valida nome
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setNome("");
            arrayList.add(usuario);
        }

        {//valida email
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setEmail("");
            arrayList.add(usuario);
        }

        {//valida
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setSobreNome("");
            arrayList.add(usuario);
        }

        {//valida
            UsuariaBean usuario = UsuariaBean.geraGeneric();
            usuario.setTelefone("");
            arrayList.add(usuario);
        }


        return arrayList;

    }

    @Test
    public void realizarCadastroValidar() throws Exception {

        page.setUsuario(usuariaBean.getUsuario());
        page.setSenha(usuariaBean.getSenha());
        page.setNome(usuariaBean.getNome());
        page.setSobreNome(usuariaBean.getSobreNome());
        page.setTelefone(usuariaBean.getTelefone());
        page.setEmail(usuariaBean.getEmail());

        page.salvar();

        Assert.assertEquals(usuariaBean.getMsgErro(), page.getMsg());
        Assert.assertEquals("", page.getUsuario());
    }
}
