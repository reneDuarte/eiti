package beans;

import anotacao.Aleatorio;


public class UsuariaBean extends BaseBean {

    @Aleatorio(msgErro = "Erro para validar o campo;Usuário")
    private String usuario;

    @Aleatorio(msgErro = "Erro para validar o campo;Senha")
    private String senha;

    @Aleatorio(msgErro = "Erro para validar o campo;Nome")
    private String nome;

    @Aleatorio(msgErro = "Erro para validar o campo;Sobre Nome")
    private String sobreNome;

    @Aleatorio(msgErro = "Erro para validar o campo;Telefone Favor digitar o valor no formato (99) 9 9999-9999", tamanho = 11, mascara = "(AA) A AAAA-AAAA" , tipoGeracao = TipoGeracao.NUMERICO)
    private String telefone;

    @Aleatorio(msgErro = "Erro para validar o campo;Email" , tamanho = 13 , mascara = "??????@????.???" , tipoGeracao = TipoGeracao.TEXTO)
    private String email;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
