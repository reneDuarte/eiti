package beans;

import anotacao.Aleatorio;

import java.lang.reflect.Field;
import java.util.List;

public class BeansUtil {

    public static  void pupularDadosAleatorios(List<Field> fields , Object destino){
        fields.parallelStream()
                .filter(field -> field.getAnnotation(Aleatorio.class) != null)
                .forEach(field -> {
                    try {
                        Aleatorio aleatorio = field.getAnnotation(Aleatorio.class);
                        field.setAccessible(true);
                        field.set(destino
                                ,Util.gerarString(aleatorio.tamanho()
                                        ,aleatorio.mascara()
                                        ,aleatorio.tipoGeracao()));


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }
}
