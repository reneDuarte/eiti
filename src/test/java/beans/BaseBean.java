package beans;

import anotacao.Aleatorio;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public abstract class BaseBean {

    private String msg = "";

    public static UsuariaBean geraGeneric() {

        UsuariaBean usuariaBean = new UsuariaBean();

        List<Field> fields = Arrays.asList(UsuariaBean.class.getDeclaredFields());
        BeansUtil.pupularDadosAleatorios(fields, usuariaBean);

        return usuariaBean;
    }

    public String getMsgErro() throws Exception {

        Field[] fields = this.getClass().getDeclaredFields();

        Integer tamanho = fields.length;

        /**
         * tenho que carantir a ordem
         */
        for (int i = 0; i < tamanho; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            Object value = field.get(this);

            if ((value == null || "".equals(value)) && field.getAnnotation(Aleatorio.class) != null) {
                Aleatorio aleatorio = field.getAnnotation(Aleatorio.class);
                msg = aleatorio.msgErro();
                break;
            }

        }

        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
