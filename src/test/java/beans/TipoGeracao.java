package beans;

public enum TipoGeracao {
    NUMERICO,
    TEXTO,
    ALFANUMERICO
}
