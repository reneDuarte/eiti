package beans;

import org.apache.commons.lang.RandomStringUtils;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;

public class Util {

    public static String gerarString(int tamanho, String mascara, TipoGeracao tipoGeracao) throws ParseException {

        String txt = "";

        if (TipoGeracao.NUMERICO.equals(tipoGeracao)) {

            txt = RandomStringUtils.randomNumeric(tamanho);

        } else if (TipoGeracao.TEXTO.equals(tipoGeracao)) {

            txt = RandomStringUtils.randomAlphabetic(tamanho);

        } else {
            /**
             * Adotado ccomo padrao
             */
            txt = RandomStringUtils.randomAlphabetic(tamanho);
        }

        txt = aplicarMascara(txt, mascara);

        return txt;
    }

    public static String aplicarMascara(String texto, String mascara) throws ParseException {



        if (mascara != null && !"".equals(mascara) && texto != null && !"".equals(texto)) {

            MaskFormatter mf = new MaskFormatter(mascara);

            mf.setValueContainsLiteralCharacters(false);
            texto = mf.valueToString(texto);

        }

        return texto;
    }
}
