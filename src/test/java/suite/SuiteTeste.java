package suite;

import br.com.demo.ApiwebApplication;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.SpringApplication;
import telas.TesteCadastroUsuario;

@RunWith(Suite.class)
@SuiteClasses({
        TesteCadastroUsuario.class
})
public class SuiteTeste {
    @BeforeClass
    public static void setUp() {
        SpringApplication.run(ApiwebApplication.class, "");
    }

}

