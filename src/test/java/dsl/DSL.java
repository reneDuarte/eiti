package dsl;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DSL {

    private WebDriver driver;

    public DSL(WebDriver driver) {
        this.driver = driver;
    }

    private void escrever(By by, String texto) {
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(texto);
    }

    private String getValue(By by) {
       return driver.findElement(by).getText();
    }

    public void escreverForName(String name_campo, String texto) {
        escrever(By.name(name_campo), texto);
    }

    public void clicarBotao(String id) {
        driver.findElement(By.id(id)).click();
    }


    public String getValueForName(String username) {
       return  getValue(By.name(username));
    }

    public String getValueForId(String id) {
        return  getValue(By.id(id));
    }

}
